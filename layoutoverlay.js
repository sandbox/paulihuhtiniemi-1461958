(function($) {
  $(document).ready(function() {
    // Create overlay image
    var imageURL = Drupal.settings.layoutoverlay['layoutoverlay_image_url'];
    var overlayImage = $('<div id="layoutoverlay-image"><img src="'+ imageURL +'"/></div>');
    $('body').append(overlayImage);
    overlayImage.draggable();

    // Create overlay configuration element with jQuery UI button and slider
    var overlayConfig = $('' +
      '<div id="layoutoverlay-config">' +
      '<div class="heading">Layout Overlay</div>' +
      '<div class="wrapper toggle"><input type="checkbox" id="layoutoverlay-toggle-button" /><label for="layoutoverlay-toggle-button">Toggle Overlay</label></div>' +
      '<div class="wrapper slider"><div>Opacity:</div><div id="layoutoverlay-slider"></div></div>' +
      '</div>');
    $('body').append(overlayConfig);

    // Overlay image on/off
    overlayImage.hide();
    $('#layoutoverlay-toggle-button').button().click(function() {
      overlayImage.toggle();
    });

    // Overlay image opacity
    $('#layoutoverlay-slider').slider({
      value: 50,
      slide: function(event, ui) {
        var opacity = ui.value / 100;
        overlayImage.css({'opacity' : opacity});
      }
    });

    // Show configuration element on hover
    $('#layoutoverlay-config .wrapper').hide();
    $('#layoutoverlay-config').hover(function() {
      $('#layoutoverlay-config .wrapper').toggle();
    })
 });
})(jQuery);
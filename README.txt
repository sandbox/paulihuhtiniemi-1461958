Layout Overlay
==============

Layout Overlay allows you to put an overlay image on top of the page. This way it's easy to compare HTML page to layout
image. Overlay image is not displayed on admin pages.

This module is heavily inspired by 'Design Overlay' feature found in Sasson theme (http://drupal.org/project/sasson).


HOW TO USE
==========

1. Install module
2. Put your layout image to somewhere where browser can load it
3. Go to Layout Overlay settings page at admin/config/development/layoutoverlay and set the image URL
4. Navigate to non-admin page and you should see the Layout Overlay element on bottom of the page with two configuration
   options: overlay toggle and overlay opacity


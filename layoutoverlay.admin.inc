<?php

/**
 * @file
 * Administration settings for Layout Overlay.
 */

/**
 * Administration settings form.
 *
 */
function layoutoverlay_admin_settings($form, &$form_state) {
  $form = array();

  $form['layoutoverlay_image_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Image URL'),
    '#default_value' => variable_get('layoutoverlay_image_url', ''),
    '#description' => t('URL of the layout image'),
  );

  return system_settings_form($form);
}
